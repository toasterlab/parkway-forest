# Welcome!

Welcome to Toasterlab’s Virtual Parkway Forest App. We’ve been working in the park to document how it is used and imagined by the community that surrounds it. You can use this app to explore the park, and check out 360 degree videos that were recorded and will be recorded all around it.

## How does it work?

Try using your fingers (or mouse) to move around.

The pins you see are the locations where events in the park have been recorded.
![instructions](instructions/location.png)  
**If you get lost, press the "home" button to center yourself on the park again.**

**If you press the "location" button and give the map permission, it will show you were you are in the park to help you explore** (don't worry, we don't collect or even see this information; it's only displayed on your personal device).

When you tap or click on a pin, it will give you access to a 360 VR video recorded in the park. If you’re on your smartphone, you will need the YouTube app to view these videos and they’ll move with you as you look around. You can even use a VR viewer like Google Carboard to be transported to that place.

If you’re on a computer, you can click and drag to change your point-of-view. It’s not as fun, but it’s still pretty cool.

We hope you like it it! We’ll be adding more videos as they’re created, so check back whenever you’re in the park to see what others have been doing.

## Download YouTube - Why?

The video in the park has been recorded in a 360 degree format, which allows you to look around as if you are really there. Unfortunately, it is not currently possible to display 360 videos on Android or iOS devices directly in the browser, so we use the official YouTube app from Google. If you don't already have it, please install the YouTube app on your device. You can still see the videos on your computer without installing anything at all if you prefer.

Most people have YouTube installed already, but if for some reason you don't, you can get it here:  
[[Android](https://play.google.com/store/apps/details?id=com.google.android.youtube&hl=en)] [[iPhone/iPad](https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8)]

## Add to Homescreen

On a smartphone, this app will work best if you add it to your phone's homescreen, then you can see it full-screen and access it directly any time you want. Here's how:

## Android (2 steps):

![android](instructions/homescreen_instructions_android.png)

## iPhone/iPad (2 steps):

![ios](instructions/homescreen_instructions_ios.png)
