# About

**Virtual Parkway Forest Park - 2022**

Virtual Parkway Forest Park is a public art project allowing community members who live near Parkway Forest Park to add immersive videos to a web app gallery of the park itself. Between July 4, 2021 and September 2, 2022, [Toasterlab](https://toasterlab.com/) loaned 360 VR Camera Kits to Parkway Forest families and individuals. Toasterlab also held in-person workshops for youth from the Parkway Forest Park community on 360 filmmaking over two weekends in July. These community members and youth explored their relationship to the park and learned about VR video, often testing the limits of the cameras in new and creative ways. The new work was launched in person at Parkway Forest Park on September 10, 2022. 


**Virtual Parkway Forest Park - 2021**

Between July 12, 2021 and September 4, 2021, Toasterlab loaned 360 VR Camera Kits and provided remote training to Parkway Forest families and individuals on filming and editing in 360 video, keeping care to follow COVID-19 protocols in place at the time. These community members explored their relationship to the park, learned about VR video, and shared their work with others. The new work was launched online on September 25, 2021. 


**Parkway Forest Park VR – 2018**

This workshop took place over two weekends at Parkway Forest Park in Summer 2018. Workshop participants devised and recorded site-specific short films in Parkway Forest Park using 360 immersive cameras. At the end of the second weekend—during the afternoon and evening of July 8—participants and the community shared their original content as part of a Pop-Up VR Cinema, in tents at the south end of the park. At the end of the summer, on the evening of August 23, 2018, the community and the participants were invited to join Toasterlab to launch a mobile app that geolocates participants’ films throughout the park. The app launch took place as part of the pre-show for the Toronto Outdoor Picture Show screening on the same evening.


**Toronto Arts Council**

All three years of these projects were produced with the support of the City of Toronto through Toronto Arts Council. 

![CCA](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/f828f61c-8fb8-4d3c-89c7-4a976fe3c5c7.png)


## Toasterlab

Toasterlab creates place-based extended reality experiences that promote deeper engagement with history, community, and imagination. Toasterlab is Ian Garrett, Justine Garrett, and Andrew Sempere. 
